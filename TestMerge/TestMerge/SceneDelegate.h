//
//  SceneDelegate.h
//  TestMerge
//
//  Created by 秦小征 on 2020/5/21.
//  Copyright © 2020 秦小征. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

